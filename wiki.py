import os
import webapp2
import jinja2
import re
import hashlib
import random
import string
import time
import hmac
import logging
from google.appengine.ext import db
from google.appengine.api import memcache

jinja_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + "/template/wiki")
)

DEBUG = True
ALPHA_NUM = re.compile('\W')
secret = "aJ)(Tjjka9)&(j"


def make_secure_val(val):
    return '%s|%s' % (val, hmac.new(secret, val).hexdigest())


def check_secure_val(secure_val):
    if secure_val:
        val = secure_val.split('|')[0]
        if secure_val == make_secure_val(val):
            return val


def make_salt(length=5):
    return ''.join(random.choice(string.letters) for x in range(length))


def make_pw_hash(name, pw, salt=None):
    if not salt:
        salt = make_salt()
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s:%s' % (h, salt)


def valid_pw(name, password, h):
    salt = h.split(':')[1]
    return h == make_pw_hash(name, password, salt)


def users_key(group='default'):
    return db.Key.from_path('users', group)


class User(db.Model):
    name = db.StringProperty(required=True)
    pw_hash = db.StringProperty(required=True)
    email = db.StringProperty()

    @classmethod
    def by_id(cls, uid):
        return User.get_by_id(int(uid), parent=users_key())

    @classmethod
    def by_name(cls, name):
        return User.gql("WHERE name=:1", name).get()

    @classmethod
    def register(cls, user, pw, mail=""):
        pwhash = make_pw_hash(user, pw)
        return User(parent=users_key(),
                    name=user,
                    pw_hash=pwhash,
                    email=mail)

    @classmethod
    def login(cls, user, pw):
        u = cls.by_name(user)
        if u and valid_pw(user, pw, u.pw_hash):
            return u
        else:
            return None


def pages_key(group='default'):
    return db.Key.from_path('pages', group)


class Page(db.Model):
    title = db.StringProperty(required=True)
    content = db.BlobProperty()
    timestamp = db.DateTimeProperty(auto_now=True)

    @classmethod
    def get_all_pages(cls, t):
        return Page.all().filter("title = ", t).order("timestamp").run()

    @classmethod
    def by_title(cls, title):
        return Page.gql("WHERE title=:1 ORDER BY timestamp DESC LIMIT 1", title).get()

    @classmethod
    def add(cls, t, c):
        page = Page(parent=pages_key(), title=t, content=c)
        page.put()
        return page

    @classmethod
    def update(cls, title, content):
        page = cls.by_title(title)
        if page:
            page.content = content
            page.put()
        return page


class WikiPage(webapp2.RequestHandler):
    def user(self):
        return check_secure_val(self.request.cookies.get("user_auth"))

    def getContent(self, title, db=False):
        return Page.by_title(title)
        # if db:
        #     p = Page.by_title(title)
        #     if p:
        #         memcache.set(title, p)
        #     return p
        # return memcache.get(title)

    def write(self, title, content, user):
        t = jinja_env.get_template("default.html")
        self.response.write(t.render({"user": user, "title": title, "content": content}))

    def get(self, page):
        if not page:
            self.write("xWiki", "Welcome to my wiki :)", self.user())
        else:
            wiki_content = self.getContent(page)
            if isinstance(wiki_content, Page):
                self.write(wiki_content.title, wiki_content.content, self.user())
            else:
                if self.user():
                    self.redirect("/_edit/" + page)
                else:
                    self.redirect('/')

    def set_secure_cookie(self, name, val):
        cookie_val = make_secure_val(val)
        self.response.headers.add_header(
            'Set-Cookie',
            '%s=%s; Path=/' % (name, cookie_val))

    def login(self, u=None, chk=None):
        if u:
            self.set_secure_cookie('user_id', str(u.key().id()))
            self.set_secure_cookie('user_auth', str(u.name))
        elif chk:
            return check_secure_val(chk)


class EditPage(WikiPage):
    def write(self, title, content, user, error=None):
        t = jinja_env.get_template("edit.html")
        self.response.write(t.render({"user": user, "title": title, "content": content, "error": error}))

    def get(self, page):
        if not self.user():
            self.redirect('/' + page)
        else:
            wiki_page = self.getContent(page, True)
            if not wiki_page:
                self.write(page, "", self.user())
            else:
                self.write(wiki_page.title, wiki_page.content, self.user())

    def post(self, title):
        logging.error(title)
        if self.user():
            content = str(self.request.get("content"))
            if self.validate_title(title):
                p = Page.add(title, content)
                # memcache.set(p.title, p.content)
                logging.error("Valid title")
                time.sleep(0.5)
                self.redirect("/" + title)
            else:
                logging.error("Invalid title")
                self.write(title, content, self.user(), "Title should be alpha numeric with no spaces")
        else:
            self.redirect('/')

    TITLE_RE = re.compile(r"^((?:[a-zA-Z0-9_-]+/?)*)$")

    def validate_title(self, key):
        return key and self.TITLE_RE.match(key)


class History(EditPage):
    def history(self, title):
        t = jinja_env.get_template("history.html")
        his = Page.get_all_pages(title)
        pages = []
        for h in his:
            pages.append(h)
        pages.reverse()
        self.response.write(t.render({
            "title": title,
            "history": pages,
            "user": self.user()
        }))

    def get(self, title):
        if not self.user():
            self.redirect('/')
        else:
            self.history(title)


class Logout(WikiPage):
    def get(self):
        self.response.headers.add_header("Set-Cookie", str("user_auth=;path=/"))
        self.response.headers.add_header("Set-Cookie", str("user_id=;path=/"))
        self.redirect('/')


class Signup(WikiPage):
    EMAIL_REGEX = re.compile(r"^[\S]+@[\S]+\.[\S]+$")
    USER_REGEX = re.compile(r"^[a-zA-Z0-9-_]{3,20}$")
    PASS_REGEX = re.compile(r"^.{3,20}$")

    def write(self, username="", email="", e_username="", e_password="", e_verify="", e_email=""):
        t = jinja_env.get_template("signup.html")
        self.response.write(t.render({"username": username, "email": email, "e_username": e_username,
                                      "e_password": e_password, "e_verify": e_verify, "e_email": e_email}))

    def get(self):
        self.write()

    def post(self):
        username = self.request.get("username")
        password = self.request.get("password")
        verify = self.request.get("verify")
        email = self.request.get("email")
        e_username = ""
        e_password = ""
        e_verify = ""
        e_email = ""
        ok = True
        if not self.validate_user(username):
            e_username = "Don't think this username will work"
            ok = False
        if not self.validate_password(password):
            e_password = "Try some better password"
            ok = False
        elif password != verify:
            e_verify = "Password should match"
            ok = False
        if not self.validate_email(email):
            e_email = "I think this is not an email address"
            ok = False
        if ok:
            u = User.by_name(username)
            if u:
                e_username = "User exists try something else"
                self.write(username, email, e_username, e_password, e_verify, e_email)
            else:
                u = User.register(username, password, email)
                u.put()
                self.login(u=u)
                self.redirect("/")
                # TODO check this url
                # self.response.write(u.name)
        else:
            self.write(username, email, e_username, e_password, e_verify, e_email)

    def validate_email(self, email):
        return (not email) or (email and self.EMAIL_REGEX.match(email))

    def validate_user(self, user):
        return user and self.USER_REGEX.match(user)

    def validate_password(self, password):
        return password and self.PASS_REGEX.match(password)


class Login(WikiPage):
    def write(self, error="", username=""):
        t = jinja_env.get_template("login.html")
        self.response.write(t.render({'error': error, 'username': username}))

    def get(self):
        self.write()

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')

        u = User.login(username, password)
        if u:
            self.login(u)
            self.redirect('/')
            #TODO check this url
        else:
            self.write("Either username or password not valid!!!", username=username)


PAGE_RE = r'((?:[a-zA-Z0-9_-]+/?)*)'
app = webapp2.WSGIApplication([
                                  ("/signup", Signup),
                                  ("/login", Login),
                                  ("/logout", Logout),
                                  ("/_edit/" + PAGE_RE, EditPage),
                                  ("/_history/" + PAGE_RE, History),
                                  ("/" + PAGE_RE, WikiPage)
                              ],
                              debug=DEBUG)