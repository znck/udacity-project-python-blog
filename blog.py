import os
import webapp2
import jinja2
import hmac
import hashlib
import random
import string
import re
import json
import time
from google.appengine.api import memcache
from google.appengine.ext import db


jinja_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)+"/template/blog"),
    autoescape=False
)

ALPHA_NUM = re.compile('\W')
secret = "aJ)(Tjjka9)&(j"


def make_secure_val(val):
    return '%s|%s' % (val, hmac.new(secret, val).hexdigest())


def check_secure_val(secure_val):
    val = secure_val.split('|')[0]
    if secure_val == make_secure_val(val):
        return val


class Post(db.Model):
    post_id = db.StringProperty(indexed=True)
    title = db.StringProperty()
    content = db.TextProperty()
    created = db.DateTimeProperty(auto_now_add=True)
    last_modified = db.DateTimeProperty(auto_now=True)


class BlogHandler(webapp2.RequestHandler):
    def write(self, posts, time=0):
        template = jinja_env.get_template('blog-list.html')
        self.response.write(template.render({'posts': posts, 'last_update': int(time)}))

    def get(self):
        t = memcache.get('last_load_time')
        posts = []
        if t:
            posts = memcache.get('front_page')
        else:
            t = time.time()
            posts = Post.all().order("-created")
            memcache.set('last_load_time', t)
            p = []
            for x in posts:
                p.append(x)
            memcache.set('front_page', p)
            posts = p
        if len(posts) > 0:
            self.write(posts, time.time() - int(t))
        else:
            self.write(posts=[{
                'title': "No posts",
                'content': "try posting something <a href=\"/blog/newpost\">here</a>"
            }])

    def set_secure_cookie(self, name, val):
        cookie_val = make_secure_val(val)
        self.response.headers.add_header(
            'Set-Cookie',
            '%s=%s; Path=/' % (name, cookie_val))

    def login(self, u=None, chk=None):
        if u:
            self.set_secure_cookie('user_id', str(u.key().id()))
        elif chk:
            return check_secure_val(chk)

    @classmethod
    def posts(cls, post_id=None):
        t = memcache.get("time, %s" % post_id)
        post = []
        if t is not None:
            p = memcache.get(post_id)
            post.append(p)
            return [time.time() - t, post]
        else:
            t = time.time()
            memcache.set("time, %s" % post_id, t)
            posts = Post.all().filter('post_id = ', post_id).run()
            post = []
            for p in posts:
                memcache.set(p.post_id, p)
                post.append(p)
            return [time.time() - t, post]
        # else:
        #     return Post.all().order("-created")


class PostHandler(webapp2.RequestHandler):
    def get(self, post_id):
        x = BlogHandler.posts(post_id)
        self.write(x[1], x[0])
        # self.response.write(post)

    def write(self, post, ts):
        t = jinja_env.get_template("blog-list.html")
        self.response.write(t.render({'posts': post, 'last_update': int(ts)}))


class NewPostHandler(webapp2.RequestHandler):
    def save(self, title, content):
        d = Post(title=title, content=content.replace('\n', '<br>'), post_id=re.sub(ALPHA_NUM, '-', title).lower())
        d.put()
        return d

    def write(self, title="", content="", error=""):
        t = jinja_env.get_template("new-post.html")
        self.response.write(t.render({
            'title': title,
            'content': content,
            'error': error
        }))

    def get(self):
        self.write()

    def post(self):
        title = self.request.get("subject")
        content = self.request.get("content")
        if title and content:
            post_id = self.save(title, content)
            self.redirect('/blog/'+str(post_id.post_id))
        else:
            self.write(title, content, error="Title and content, both required")


def make_salt(length=5):
    return ''.join(random.choice(string.letters) for x in range(length))


def make_pw_hash(name, pw, salt=None):
    if not salt:
        salt = make_salt()
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s:%s' % (h, salt)


def valid_pw(name, password, h):
    salt = h.split(':')[1]
    return h == make_pw_hash(name, password, salt)


def users_key(group='default'):
    return db.Key.from_path('users', group)


class User(db.Model):
    name = db.StringProperty(required=True)
    pw_hash = db.StringProperty(required=True)
    email = db.StringProperty()

    @classmethod
    def by_id(cls, uid):
        return User.get_by_id(int(uid), parent=users_key())

    @classmethod
    def by_name(cls, name):
        return User.gql("WHERE name=:1", name).get()


    @classmethod
    def register(cls, user, pw, mail=""):
        pwhash = make_pw_hash(user, pw)
        return User(parent=users_key(),
                    name=user,
                    pw_hash=pwhash,
                    email=mail)

    @classmethod
    def login(cls, user, pw):
        u = cls.by_name(user)
        if u and valid_pw(user, pw, u.pw_hash):
            return u
        else:
            return None


class SignupHandler(BlogHandler):
    EMAIL_REGEX = re.compile(r"^[\S]+@[\S]+\.[\S]+$")
    USER_REGEX = re.compile(r"^[a-zA-Z0-9-_]{3,20}$")
    PASS_REGEX = re.compile(r"^.{3,20}$")

    def write(self, username="", email="", e_username="", e_password="", e_verify="", e_email=""):
        t = jinja_env.get_template("signup.html")
        self.response.write(t.render({"username": username, "email": email, "e_username": e_username,
                                      "e_password": e_password, "e_verify": e_verify, "e_email": e_email}))

    def get(self):
        self.write()

    def post(self):
        username = self.request.get("username")
        password = self.request.get("password")
        verify = self.request.get("verify")
        email = self.request.get("email")
        e_username = ""
        e_password = ""
        e_verify = ""
        e_email = ""
        ok = True
        if not self.validate_user(username):
            e_username = "Don't think this username will work"
            ok = False
        if not self.validate_password(password):
            e_password = "Try some better password"
            ok = False
        elif password != verify:
            e_verify = "Password should match"
            ok = False
        if not self.validate_email(email):
                e_email = "I think this is not an email address"
                ok = False
        if ok:
            u = User.by_name(username)
            if u:
                e_username = "User exists try something else"
                self.write(username, email, e_username, e_password, e_verify, e_email)
            else:
                u = User.register(username, password, email)
                u.put()
                self.login(u=u)
                self.redirect("/blog/welcome")
                # self.response.write(u.name)
        else:
            self.write(username, email, e_username, e_password, e_verify, e_email)

    def validate_email(self, email):
        return (not email) or (email and self.EMAIL_REGEX.match(email))

    def validate_user(self, user):
        return user and self.USER_REGEX.match(user)

    def validate_password(self, password):
        return password and self.PASS_REGEX.match(password)


class LoginHandler(BlogHandler):
    def write(self, error="", username=""):
        t = jinja_env.get_template("login.html")
        self.response.write(t.render({'error': error, 'username': username}))

    def get(self):
        self.write()

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')

        u = User.login(username, password)
        # usr = User.by_name(username)
        # self.response.write(usr.name+"<br>")
        if u:
            self.login(u)
            self.redirect('/blog/welcome')
        else:
            self.write("Either username or password not valid!!!", username=username)


class LogoutHandler(webapp2.RequestHandler):
    def get(self):
        self.response.headers.add_header("Set-Cookie", str("user_auth=;path=/"))
        self.response.headers.add_header("Set-Cookie", str("user_id=;path=/"))
        self.redirect('/blog/signup')


class FlushHandler(webapp2.RequestHandler):
    def get(self):
        memcache.flush_all()
        self.redirect('/blog')


class WelcomeHandler(BlogHandler):
    def get(self):
        user = self.request.cookies.get("user_id")
        id =''
        if user and self.login(chk=user):
            user = user.split('|')[0]
            id = int(user)
            u = User.by_id(int(user))
            if u:
                self.response.write("Welcome "+u.name)
            else:
                self.response.write("Welcome %s" % id)
        else:
            self.redirect("/blog/login")


class JsonHandler(PostHandler):
    def get(self, post_id):
        p = BlogHandler.posts(post_id)
        post = []
        if p:
            for x in p:
                a = {'title': x.title, 'content': x.content, 'date_created': x.created.strftime("%b %d %Y %H:%M:%S"),
                     'last_modified': x.last_modified.strftime("%b %d %Y %H:%M:%S")}
                post.append(a)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps(post))


class Json2Handler(webapp2.RequestHandler):
    def get(self):
        p = Post.all().order('-created')
        post = []
        if p:
            for x in p:
                a = {'title': x.title, 'content': x.content, 'date_created': x.created.strftime("%b %d %Y %H:%M:%S"),
                     'last_modified': x.last_modified.strftime("%b %d %Y %H:%M:%S")}
                post.append(a)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps(post))


app = webapp2.WSGIApplication([
    (r'/blog/?', BlogHandler),
    (r'/blog/?\.json', Json2Handler),
    (r'/blog/newpost/?', NewPostHandler),
    ("/blog/signup/?", SignupHandler),
    ("/blog/login/?", LoginHandler),
    ("/blog/logout/?", LogoutHandler),
    ("/blog/flush/?", FlushHandler),
    ("/blog/welcome/?", WelcomeHandler),
    (r'/blog/newpost\.json', NewPostHandler),
    ("/blog/signup\.json", SignupHandler),
    ("/blog/login\.json", LoginHandler),
    ("/blog/logout\.json", LogoutHandler),
    ("/blog/welcome\.json", WelcomeHandler),
    ("/blog/([0-9a-zA-Z-]+)\.json$", JsonHandler),
    ('/blog/([0-9a-zA-Z-]+)', PostHandler)

], debug=True)